##Project Install
-   Fork / Clone repository
-   Install Node.js locally (version: v8.9.1 - npm v5.5.1)
-   Install latest version of npm globally
    - npm install -g npm
-   Install Gulp Client Globally
    - npm install -g gulp (gulp@3.9.1)
-   Install Node packages
    - npm install
-   Run Gulp default task
    -   $ gulp serve
    -   Opens / Serves Index.html

##Project Deployment:
-   Run Gulp default task to build the project
    -   $ gulp build or simply $ gulp
-   Run Gulp deployment task
    -   $ gulp deploy
    -   Results 'dist' folder with compiled files only

```
dist/
    ├── fonts
    ├── images
    ├── scripts
    │   ├── scripts.js
    │   └── vendor
    │       └── vendor.js
    └── styles
        ├── styles.css
        └── vendor
            └── vendor.css
```

    -   Obs. These files will be referenced on the CMS template as:
        -   E.g. http://www.moneysmart.gov.au/moneysmartapps/[AppName]/images/image.png
        -   E.g. http://www.moneysmart.gov.au/moneysmartapps/[AppName]/scripts/scripts.js
        -   And so on...

##Notes
You will notice a few important things in the boilerplate:

-   It serves/runs the app using a MoneySmart template (CMS equivalent, main scripts and styles). This is how the app is going to be hosted in the future.
-   MoneySmart  template compatibility (css and script reset)
-   Accessibility (AA compliance)
-   Browser compatibility (TBD)
-   Minified files for deployment (view html for output files)
-   Partials, images and other files relative path

Not so important and flexible:

-   Task runner (Gulp, Grunt, Ant or any build script) and its tasks
-   App folder structure
-   JavaScript frameworks (AngularJS, ExpressJS, MeteorJS, ReactJS, etc)
-   SASS/LESS
-   CSS frameworks
-   HTML JavaScript template engine (Handlebars, Jade, Mustache, etc)
-   CSS identifiers
-   Package Manager (Bower, npm, etc)

##Build
    - true