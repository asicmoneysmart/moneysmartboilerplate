/**
 * Created by marcoramires on 26/08/2016.
 */

$(document).ready(function () {
    var app, version, app_shell, scope, manifest;
    manifest = $('link[rel="manifest"]').attr('href');
    var jqxhr = $.getJSON(manifest, function (data) {
        app = data.name;
        version = data.version;
        app_shell = data.app_shell;
        scope = data.scope;
    }).done(function () {
        var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;
        var open = indexedDB.open(app, 1);
        // Create the schema
        open.onupgradeneeded = function () {
            var db = open.result;
            db.createObjectStore("AppSettings", {keyPath: "type"});
        };
        open.onsuccess = function () {
            // Start a new transaction
            var db = open.result;
            var tx = db.transaction("AppSettings", "readwrite");
            var store = tx.objectStore("AppSettings");

            // Add some data
            store.put({type: "version", value: version});
            store.put({type: "files", value: app_shell});

            // Close the db when the transaction is done
            tx.oncomplete = function () {
                db.close();
            };
        };
    }).always(function () {
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker
                .register('../../service-worker.js?app=' + app + '&version=' + version, {
                    scope: scope
                })
                .then(function (registration) {

                    if (!navigator.serviceWorker.controller) {
                        return;
                    }

                    if (registration.installing) {
                        console.log('[ServiceWorker] Installing');
                        registration.installing.addEventListener('statechange', function () {
                            if (this.state === 'installed') {
                                //TODO: Popup event
                                alert('New version available. Click \'OK\' to update the app.');
                                this.postMessage({action: 'skipWaiting'});
                            }
                        });
                        return;
                    }

                    if (registration.waiting) {
                        console.log('[ServiceWorker] Installed');
                        //TODO: Popup event
                        return;
                    }

                    if (registration.active) {
                        console.log('[ServiceWorker] Active');
                    }

                    console.log('[ServiceWorker] Registration successful with scope:', registration.scope);

                }).catch(function (error) {
                console.log('[ServiceWorker] Registration failed with ' + error);
            });

            navigator.serviceWorker.addEventListener('controllerchange', function () {
                window.location.reload();
            });
        }
    });
});

