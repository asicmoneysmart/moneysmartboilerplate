var appSettings = (self.location.search).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
const request = indexedDB.open(appSettings.app, 1);
var db, cacheName, app_shell;

request.onsuccess = function() {
    db = this.result; // Average 8ms
};

self.addEventListener('install', function (event) {
    console.log('[ServiceWorker] Install');
    const response = new Promise(function (resolve) {
        setTimeout(function () {
            var tx = db.transaction("AppSettings", "readwrite");
            var store = tx.objectStore("AppSettings");
            var files = store.get('files');
            var version = store.get('version');

            tx.onabort = function() { reject(tx.error); };
            tx.oncomplete = function() {
                app_shell = files.result.value;
                cacheName = appSettings.app + '-cache-' + version.result.value;
                db.close();
                resolve();
            };
        }, 1000)
    });

    event.waitUntil(response.then(function() {
        caches.open(cacheName).then(function (cache) {
            return cache.addAll(app_shell);
        })
    }));
});

self.addEventListener('activate', function (event) {
    console.log('[ServiceWorker] Activate');
    event.waitUntil(
        caches.keys().then(function (keyList) {
            return Promise.all(
                keyList.filter(function (key) {
                    return key.startsWith(appSettings.app) && key !== cacheName;
                }).map(function (key) {
                    console.log('[ServiceWorker] Removing old cache', key);
                    return caches.delete(key);
                })
            );
        })
    );
});

self.addEventListener('message', function (event) {
    console.log('[ServiceWorker] Message');
    if (event.data.action === 'skipWaiting') {
        self.skipWaiting();
    }
});

// Functional Events
// -----------------
self.addEventListener('fetch', function (event) {
    // console.log('[ServiceWorker] Fetch', event.request.url);
    //TODO: Fetch offline files - Replace to AWS scripts
    var standalone = [
        'https://s3-ap-southeast-2.amazonaws.com/',
        'https://d11b5fc6vsbqbg.cloudfront.net/',
        'https://ajax.googleapis.com/'
    ];

    if (event.request.url.indexOf(standalone[0]) !== -1 ||
        event.request.url.indexOf(standalone[1]) !== -1) {
        if(event.request.url.indexOf('manifest') !== -1) {
            return;
        } else {
            fetch(event.request)
                .then(function (response) {
                    return caches.open(cacheName).then(function (cache) {
                        cache.put(event.request.url, response.clone());
                        console.log('[ServiceWorker] Fetched & Cached Data', event.request.url);
                    });
                })
        }
    }

    event.respondWith(
        caches.match(event.request).then(function (response) {
            return response || fetch(event.request);
        })
    );
});

self.addEventListener('sync', function (event) {
//TODO
});

self.addEventListener('push', function (event) {
//TODO
});

