**Progressive App Prototype**

***Project Install***
* Clone repository
* Install [Webserver for Chrome](https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb)
* Run app from launcher and select [progressive-site] folder
* Go to [http://127.0.0.1:8887](http://127.0.0.1:8887)

***Debug Android***
* Open chrome://inspect/#devices
* Tick Discover USB devices
* Port forwarding
* Add [http://127.0.0.1:8887](http://127.0.0.1:8887)
* Open [http://127.0.0.1:8887](http://127.0.0.1:8887) on device
* Inspect [http://127.0.0.1:8887](http://127.0.0.1:8887) on DevTools
* View 'Application' tab

***Site structure***
```sh
./root
| service-worker.js
/apps/[app-name]
/apps/[app-name]/[app-shell]
/apps/[app-name]/manifest-[app-name].json
```

***Reference manisfest.json in App***
```
<head>
<link rel="manifest" href="static-moneysmart/apps/[app-name]/manifest-[app-name].json">
</head>
```

***Get Service Worker details as a promise***
```javascript
var app, version, app_shell;
var jqxhr = $.getJSON("./standalone/[app-name]/manifest.json", function (data) {
    app = data.name;
    version = data.version;
    app_shell = data.app_shell;
}).done(function () {
    //TODO:
}).always(function () {
    if ('serviceWorker' in navigator) {
        //TODO:
    }
}); 
```
***Save Service Worker details into a IndexedDB***
`Suggested by Jake Archibald ‏@jaffathecake `
```javascript
var app, version, app_shell;
 var jqxhr = $.getJSON("./standalone/[app-name]/manifest.json", function (data) {
     app = data.name;
     version = data.version;
     app_shell = data.app_shell;
 }).done(function () {
var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;
    var open = indexedDB.open(app, 1);
    // Create the schema
    open.onupgradeneeded = function () {
        var db = open.result;
        db.createObjectStore("AppSettings", {keyPath: "type"});
    };
    open.onsuccess = function () {
        // Start a new transaction
        var db = open.result;
        var tx = db.transaction("AppSettings", "readwrite");
        var store = tx.objectStore("AppSettings");

        // Add some data
        store.put({type: "version", value: version});
        store.put({type: "files", value: app_shell});

        // Close the db when the transaction is done
        tx.oncomplete = function () {
            db.close();
        };
    };
 }).always(function () {
     if ('serviceWorker' in navigator) {
         //TODO:
     }
 }); 
```
***Register Service Worker***
```javascript
if ('serviceWorker' in navigator) {
            navigator.serviceWorker
                .register('../../service-worker.js?app=' + app + '&version=' + version, {
                    scope: '/tools-and-resources/calculators-and-apps/calculator.html'
                })
                .then(function (registration) {

                    if(!navigator.serviceWorker.controller) {
                        return;
                    }

                    if (registration.installing) {
                        console.log('[ServiceWorker] Installing');
                        registration.installing.addEventListener('statechange', function () {
                            if (this.state === 'installed') {
                                //TODO: Popup event
                                //this.postMessage({action: 'skipWaiting'});
                            }
                        });
                        return;
                    }

                    if (registration.waiting) {
                        console.log('[ServiceWorker] Installed');
                        //TODO: Popup event
                        return;
                    }

                    if (registration.active) {
                        console.log('[ServiceWorker] Active');
                    }

                    console.log('[ServiceWorker] Registration successful with scope:', registration.scope);

                }).catch(function (error) {
                    console.log('[ServiceWorker] Registration failed with ' + error);
                });

            navigator.serviceWorker.addEventListener('controllerchange', function() {
                window.location.reload();
            });
    }
```
***PWA Resources***

* [https://jakearchibald.com/2014/offline-cookbook/](https://jakearchibald.com/2014/offline-cookbook/)
* [https://scotch.io/tutorials/the-ultimate-guide-to-progressive-web-applications](https://scotch.io/tutorials/the-ultimate-guide-to-progressive-web-applications)
* [Intro to Progressive Web Apps by Google](https://www.udacity.com/course/intro-to-progressive-web-apps--ud811) by Udacity (Free)
* [Offline Web Applications by Google](https://www.udacity.com/course/offline-web-applications--ud899) by Udacity (Free)
* [Jake Archibald](https://twitter.com/jaffathecake)
