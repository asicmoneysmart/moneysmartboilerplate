/**
 * Modified by MoneySmart on 14/11/17.
 * Based on Yeoman Gulp WebApp
 */

var env = {
    app: 'app',
    dev: '.tmp',
    dist: 'dist',
    test: 'test',
    money_smart_app: 'MoneySmartBoilerplate' /* !!!! Important !!! */
};

var paths = {
    sass: env.app + '/sass',
    css_vendor: env.app + '/css/vendor',
    styles: env.dev + '/styles',

    js: env.app + '/js',
    js_vendor: env.app + '/js/vendor',
    scripts: env.dev + '/scripts',
    scripts_vendor: env.dev + '/scripts/vendor',

    fonts: env.app + '/fonts',
    fonts_comp: env.dist + '/fonts',

    images: env.app + '/images',
    images_comp: env.dist + '/images'
};

var gulp = require('gulp');
var path = require('path');
var merge = require('merge-stream');
var sourcemaps = require('gulp-sourcemaps');
var runSequence = require('run-sequence');
var $ = require('gulp-load-plugins')();

/* -------- Development tasks --------- */

gulp.task('scripts', function () {
    gulp.src([paths.js + '/*.js'])
        .pipe(sourcemaps.init())
        .pipe($.concat('scripts.js'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(paths.scripts));
});

gulp.task('scripts_vendor', function () {
    gulp.src([
        paths.js_vendor + '/**/*.js'
        //Inject vendor modules here...
        // 'node_modules/handlebars/dist/handlebars.runtime.js'
    ])
        .pipe($.concat('vendor.js'))
        .pipe(gulp.dest(paths.scripts_vendor));
});

gulp.task('styles', function () {
    gulp.src(paths.sass + '/styles.scss')
        .pipe(sourcemaps.init())
        .pipe($.sass())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(paths.styles));
});

gulp.task('styles_vendor', function () {
    gulp.src([paths.css_vendor + '/*.min.css'])
        .pipe($.concat('vendor.css'))
        .pipe(gulp.dest(paths.styles + '/vendor'));
});

gulp.task('watch', ['connect'], function () {
    //TODO: Review this
    $.livereload.listen();
    gulp.watch([
        paths.styles + '/*.css',
        paths.scripts + '/*.js'
    ]).on('change', $.livereload.changed);

    gulp.watch(paths.sass + '/**/*.scss', ['styles']);
    gulp.watch([paths.js + '/*.js', paths.scripts + '/*.js'], ['scripts']);
});

gulp.task('connect', function () {

    var callback = function () {
        var serveStatic = require('serve-static');
        var serveIndex = require('serve-index');

        var app = require('connect')()
            .use(require('connect-livereload')({port: 35729}))
            .use(serveStatic('.tmp'))
            .use(serveStatic('app'))
            .use(serveIndex('app'));

        require('opn')('http://localhost:9000');

        require('http').createServer(app)
            .listen(9000)
            .on('listening', function () {
                console.log('Started connect web server on http://localhost:9000');
            });
    };

    runSequence(['scripts', 'scripts_vendor'], ['styles', 'styles_vendor'], callback);
});

gulp.task('serve', function () {
    runSequence(['connect', 'watch']);
});

/* -------- Deployment tasks --------- */

gulp.task('fonts', function () {
    return gulp.src(paths.fonts)
        .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
        .pipe($.flatten())
        .pipe(gulp.dest(paths.fonts_comp));
});

gulp.task('images', function () {
    gulp.src(paths.images + '/**/*')
        .pipe($.cache($.imagemin({
            progressive: true,
            interlaced: true
        })))
        .pipe(gulp.dest(paths.images_comp));
});

gulp.task('assets', function () {

    gulp.src('.tmp/scripts/scripts.js')
        .pipe($.uglify())
        .pipe(gulp.dest('dist/scripts'));

    gulp.src('.tmp/scripts/vendor/*.js')
        .pipe($.uglify())
        .pipe(gulp.dest('dist/scripts/vendor'));

    gulp.src('.tmp/styles/**/*.css')
        .pipe($.csso())
        .pipe(gulp.dest('dist/styles'));

    gulp.src('app/app-data/*')
        .pipe(gulp.dest('dist/app-data'));
});

gulp.task('deploy', function () {
    var callback = function () {
        setTimeout(function () {
            gulp.src(env.dist + '/**/*')
                .pipe($.size({title: 'Build Package Size:', gzip: true}));
        }, 3000);
    };

    runSequence('assets', 'images', 'fonts', callback);
});

gulp.task('build', function () {
    runSequence('jshint', ['scripts', 'scripts_vendor'], ['styles', 'styles_vendor']);
});

gulp.task('default', function () {
    runSequence('clean', 'build');
});

/* -------- Helper tasks --------- */

//Delete '.tmp' and 'dist' folder for a clean build
gulp.task('clean', require('del').bind(null, ['.tmp', 'dist']));

/* -------- Test tasks --------- */

gulp.task('test', function () {
    //TODO:
});

gulp.task('jshint', function () {
    return gulp.src(paths.js + '/**/.js')
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish'))
        .pipe($.jshint.reporter('fail'));
});